<!DOCTYPE html>
    <html>
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>credit-card-checker</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <input type="text" id="credit-card" value="4691 4195 2649 6607" />
        <button onClick="checkInput()">Check!</button>
        <script src="credit-card-checker.js"></script>
        <script>
            function checkInput() {
                if (creditCardIsValid($('#credit-card').val())) {
                    alert('The card is valid!');
                } else {
                    alert('The card is NOT valid!');
                }
            }
        </script>
    </body>
</html>