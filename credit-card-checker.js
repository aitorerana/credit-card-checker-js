function creditCardIsValid(cardNumber) {
    cardNumber = cardNumber.replace(/\s/g, '');
    const [odd, even] = [...cardNumber].reduce((r, char, i) => (r[i % 2].push(char), r), [[],[]])
    const oddDouble = odd.map(doubleArray).map(sumChars);
    const evenAdd = even.reduce(sumArray, 0);
    const oddDoubleAdd = oddDouble.reduce(sumArray, 0);
    return (evenAdd + oddDoubleAdd) % 10 == 0;
}

const sumArray = (carry, item) => carry + parseInt(item);
const sumChars = number => String(number).split('').reduce(sumArray, 0);
const doubleArray = number => parseInt(number) * 2;