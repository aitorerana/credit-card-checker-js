# credit-card-checker #

This is jQuery library with a function to check if a credit card is valid.

### Requirements ###

This library requires to import **[jQuery](https://jquery.com/ "jQuery")** before calling the function.

### Configuration ###

1. Import the script: ```<script src="credit-card-checker.js"></script>```
2. Use the JS function: ```creditCardIsValid(cardNumber);```

### Run the project ###

Here a simple command to start this project localy: **php -S localhost:7000**

### Run the project with Docker ###

Run the command: **docker-compose up -d**

### Author ###

[Aitor Eraña](http://cv.aitorerana.com/ "CV aitorerana")